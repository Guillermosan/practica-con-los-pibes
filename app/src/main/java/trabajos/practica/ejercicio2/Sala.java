package trabajos.practica.ejercicio2;

public class Sala {
    private boolean completa;

    public Sala (boolean completa){
        
        this.completa = completa;

    }

    public void disponible(){

    }

    public boolean isCompleta() {
        return completa;
    }

    public void setCompleta(boolean completa) {
        this.completa = completa;
    }

}
