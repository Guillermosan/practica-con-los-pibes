package trabajos.practica.ejercicio2;

public class Espectador {
    private String nombre;
    private int edad;
    private int dinero_cuenta;

    public Espectador(String nombre, int edad, int dinero_cuenta){

        this.dinero_cuenta = dinero_cuenta;
        this.edad = edad;
        this.nombre = nombre;

      }

    public String getNombre() {
      return nombre;
    }

    public void setNombre(String nombre) {
      this.nombre = nombre;
    }

    public int getEdad() {
      return edad;
    }

    public void setEdad(int edad) {
      this.edad = edad;
    }

    public int getDinero_cuenta() {
      return dinero_cuenta;
    }

    public void setDinero_cuenta(int dinero_cuenta) {
      this.dinero_cuenta = dinero_cuenta;
    }

  

}
