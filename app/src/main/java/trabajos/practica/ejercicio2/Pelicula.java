package trabajos.practica.ejercicio2;

public class Pelicula {
    private String genero;
    private String titulo;
    private int duracion;
    private int edad_minima;
    private String actores;
    private String director;

    public Pelicula(String genero, String titulo, int duracion, int edad_minima,
      String actores, String director){

        this.actores = actores;
        this.director = director;
        this.duracion = duracion;
        this.edad_minima = edad_minima;
        this.titulo = titulo;
        this.genero = genero;

    }

    public String getGenero() {
      return genero;
    }

    public void setGenero(String genero) {
      this.genero = genero;
    }

    public String getTitulo() {
      return titulo;
    }

    public void setTitulo(String titulo) {
      this.titulo = titulo;
    }

    public int getDuracion() {
      return duracion;
    }

    public void setDuracion(int duracion) {
      this.duracion = duracion;
    }

    public int getEdad_minima() {
      return edad_minima;
    }

    public void setEdad_minima(int edad_minima) {
      this.edad_minima = edad_minima;
    }

    public String getActores() {
      return actores;
    }

    public void setActores(String actores) {
      this.actores = actores;
    }

    public String getDirector() {
      return director;
    }

    public void setDirector(String director) {
      this.director = director;
    }

      

}
