package trabajos.practica.ejercicio2;

import java.time.LocalDate;
import java.time.LocalTime;

public class Entrada {
    
    private int nro_ticket;
    private int num_funcion;
    private int precio;
    private LocalDate fecha_venta;
    private String nombre_pelicula;
    private LocalDate fecha_funcion;
    private LocalTime hora_funcion;
    private String clasificacion_peli;

    // Creado para el propósito de testing
    public Entrada(){

    }

    public Entrada(int nro_ticket, int num_funcion, String nombre_pelicula, int precio, LocalDate fecha_funcion,
    LocalDate fecha_venta,LocalTime hora_funcion, String clasificacion_peli){

        this.clasificacion_peli = clasificacion_peli;
        this.fecha_funcion = fecha_funcion;
        this.fecha_venta = fecha_venta;
        this.hora_funcion = hora_funcion;
        this.nombre_pelicula = nombre_pelicula;
        this.nro_ticket = nro_ticket;
        this.num_funcion = num_funcion;
        this.precio = precio;
     
    }

    public int getNro_ticket() {
        return nro_ticket;
    }

    public void setNro_ticket(int nro_ticket) {
        this.nro_ticket = nro_ticket;
    }

    public int getNum_funcion() {
        return num_funcion;
    }

    public void setNum_funcion(int num_funcion) {
        this.num_funcion = num_funcion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public LocalDate getFecha_venta() {
        return fecha_venta;
    }

    public void setFecha_venta(LocalDate fecha_venta) {

        if (fecha_venta == null) {
            throw new NullPointerException("Fecha de venta no puede ser nula");
        }

        // Puede venderse a futuro, pero para testear a ver
        if (fecha_venta.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Fecha de venta no puede ser en el futuro");
        }

        this.fecha_venta = fecha_venta;
    }

    public String getNombre_pelicula() {
        return nombre_pelicula;
    }

    public void setNombre_pelicula(String nombre_pelicula) {
        this.nombre_pelicula = nombre_pelicula;
    }

    public LocalDate getFecha_funcion() {
        return fecha_funcion;
    }

    public void setFecha_funcion(LocalDate fecha_funcion) {
        this.fecha_funcion = fecha_funcion;
    }

    public LocalTime getHora_funcion() {
        return hora_funcion;
    }

    public void setHora_funcion(LocalTime hora_funcion) {
        this.hora_funcion = hora_funcion;
    }

    public String getClasificacion_peli() {
        return clasificacion_peli;
    }

    public void setClasificacion_peli(String clasificacion_peli) {
        this.clasificacion_peli = clasificacion_peli;
    }

    

}
