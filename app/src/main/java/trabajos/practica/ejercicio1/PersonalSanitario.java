package trabajos.practica.ejercicio1;
public class PersonalSanitario extends Persona {
    protected int nro_empleado;

    public PersonalSanitario(String nombre, String apellido, int dni, int nro_empleado){
    
        super(nombre, apellido, dni);
        this.nro_empleado = nro_empleado;

    }

    public int getNro_empleado() {
        return nro_empleado;
    }

    public void setNro_empleado(int nro_empleado) {
        this.nro_empleado = nro_empleado;
    }

}
