package trabajos.practica.ejercicio1;

public class HistoriaClinica {
    private int nro;
    private String fecha;

    public HistoriaClinica(int nro, String fecha){

        this.fecha = fecha;
        this.nro = nro;

    }


    public int getNro() {
        return nro;
    }

    public void setNro(int nro) {
        this.nro = nro;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    

}
