package trabajos.practica.ejercicio1;
public class Enfermero extends PersonalSanitario {
    
    public Enfermero(String nombre, String apellido, int dni, int nro_empleado){
       
        // EN una super no es necesario declarar el tipo de dato
        // yo uso el constructor de las clases superiores
        super(nombre, apellido, dni, nro_empleado);
    
    }
}
