package trabajos.practica.ejercicio1;
public class Medico extends PersonalSanitario {
    
     private String especialidad;

     public Medico(String nombre, String apellido, int dni, int nro_empleado, String especialidad){

     super(nombre, apellido, dni, nro_empleado);
     this.especialidad = especialidad;

     }

     public String getEspecialidad() {
          return especialidad;
     }

     public void setEspecialidad(String especialidad) {
          this.especialidad = especialidad;
     }
}
